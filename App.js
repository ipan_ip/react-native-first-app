import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { 
  StyleSheet, View, Text, 
} from 'react-native';
import Tugas12 from './tugas/tugas12/App';
import Tugas13LoginScreen from './tugas/tugas13/LoginScreen';
import Tugas13AboutScreen from './tugas/tugas13/AboutScreen';
import Tugas14 from './tugas/tugas14/Tugas14/App';
// import Tugas15A from './tugas/tugas15/index';
// import Tugas15B from './tugas/TugasNavigation/index';
import Quiz3 from './Quiz3/index';

export default function App() {
  return (
    <Quiz3 />
  );
}

const styles = StyleSheet.create({
});

import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { 
  StyleSheet, 
  Text, 
  Image, 
  View, 
  ScrollView,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class VideoItem extends Component {
  render() {
    let video = this.props.video;
    return (
      <View style={styles.container}>
        <Image 
          source={{uri:video.snippet.thumbnails.medium.url}}
          // source={require('../images/logo.png')}
          style={{height:200}}
        />
        <View style={styles.descContainer}>
          <Image 
            source={{uri:'https://randomuser.me/api/portraits/men/0.jpg'}}
            style={{height:50, width: 50, borderRadius: 25}}
          />
          <View style={styles.videoDetails}>
            <Text numberOfLines={2} style={styles.videoTitle}>{video.snippet.title}</Text>
            <Text style={styles.videoStats}>{video.snippet.channelTitle + " - " + nFormatter(video.statistics.viewCount) + " views " + ". 3 months ago"}</Text>
          </View>
          <TouchableOpacity>
            <Icon name="more-vert" size={20} style={{color: '#888888'}}></Icon>
          </TouchableOpacity>
        </View>
      </View>
    )
  };
}

function nFormatter(num) {
  return Math.abs(num) > 999 ? Math.sign(num)*((Math.abs(num)/1000).toFixed(1)) + 'k' : Math.sign(num)*Math.abs(num)
}

const styles = StyleSheet.create({
  container: {
    padding: 15
  },
  descContainer: {
    flexDirection: 'row',
    paddingTop: 15
  },
  videoTitle: {
    fontSize: 16,
    color: '#3c3c3c'
  },
  videoDetails: {
    paddingHorizontal: 15,
    flex: 1
  },
  videoStats: {
    fontSize: 14,
    color: '#888888'
  },
  image: {
    width: 200,
    height: 200,
  },
});

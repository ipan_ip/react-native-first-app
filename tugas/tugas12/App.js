import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { 
  StyleSheet, 
  Text, 
  Image, 
  View, 
  ScrollView,
  TouchableOpacity,
  FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './components/VideoItem'
import data from './data.json'

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.navBar}>
        <Image 
          source={require('./images/logo.png')}
          style={styles.image}
        />
        <View style={styles.rightNav}>
          <TouchableOpacity>
            <Icon name="search" style={styles.navItem} size={25}></Icon>
          </TouchableOpacity>
          <TouchableOpacity>
            <Icon name="account-circle" style={styles.navItem} size={25}></Icon>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.body}>
        <FlatList 
          data={data.items}
          renderItem={(video) =>
            <VideoItem video={video.item}/>
          }
          keyExtractor={(item) => item.id}
          ItemSeparatorComponent={() => <View style={{
            height: 0.5,
            backgroundColor: '#e5e5e5'
          }}></View>}
        />
      </View>
      <View style={styles.tabBar}>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="home" size={25} style={{color: 'red'}}></Icon>
          <Text style={styles.tabTitle, {color: 'red'}}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="whatshot" size={25}></Icon>
          <Text style={styles.tabTitle}>Trending</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="subscriptions" size={25}></Icon>
          <Text style={styles.tabTitle}>Subscriptions</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="folder" size={25}></Icon>
          <Text style={styles.tabTitle}>Library</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navBar: {
    height: 55,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:'space-between',
    marginTop: 10
  },
  rightNav: {
    flexDirection: 'row'
  },
  navItem: {
    marginLeft: 25
  },
  image: {
    width: 98,
    height: 22
  },
  body: {
    flex: 1
  },
  tabBar: {
    flexDirection: 'row',
    justifyContent: "space-around",
    height: 60,
    backgroundColor: 'white',
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5'
  },
  tabItem: {
    alignItems: "center",
    justifyContent: "center"
  },
  tabTitle: {
    fontSize: 11,
    color: '#3C3C3C',
    paddingTop: 3
  }
});

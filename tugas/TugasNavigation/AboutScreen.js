import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { 
  StyleSheet, 
  Text, 
  TextInput,
  Image, 
  View, 
  ScrollView,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import CommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class LoginScreen extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: ''
    };
  }
  render() {
    return (
    <View style={styles.container}>
      {/* <Image 
          source={require('./assets/images/pp.jpeg')}
          style={{height:100, width: 100, borderRadius: 50}}
        /> */}
      <View style={styles.tabBar}>
        <TouchableOpacity style={styles.tabItem}>
          <CommunityIcon name="account-outline" size={25} style={{color: 'red'}}></CommunityIcon>
          <Text style={styles.tabTitle, {color: 'red'}}>Account</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="directions-bike" size={25}></Icon>
          <Text style={styles.tabTitle}>Skills</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <CommunityIcon name="logout" size={25}></CommunityIcon>
          <Text style={styles.tabTitle}>Logout</Text>
        </TouchableOpacity>
      </View>
    </View>
    )
  };
}
const primaryColor = '#2F80ED';
const styles = StyleSheet.create({  
  container: {
    // padding: 15,
    flexGrow: 1,
    justifyContent:'flex-start',
    // alignItems: 'center'
  },
  tabBar: {
    flexDirection: 'row',
    justifyContent: "space-around",
    height: 60,
    backgroundColor: 'white',
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5'
  },
  tabItem: {
    alignItems: "center",
    justifyContent: "center"
  },
  tabTitle: {
    fontSize: 11,
    color: '#3C3C3C',
    paddingTop: 3
  }
});

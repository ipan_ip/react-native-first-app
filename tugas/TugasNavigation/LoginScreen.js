import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { 
  StyleSheet, 
  Text, 
  TextInput,
  Image, 
  View, 
  ScrollView,
  TouchableOpacity
} from 'react-native';

export default class LoginScreen extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: ''
    };
  }
  render() {
    return (
      <View style={styles.container}>
          <View style={{marginTop: 100}}>
            <Text style={styles.logoContainer}>
              <Text style={styles.logoImage}>P</Text>
              <Text style={styles.logoText}>ORTOFOLIO</Text>
            </Text>
          </View>
          <View style={{marginVertical: 50}}>
            <TextInput
              placeholder="Email"
              style={styles.inputBox}
              onChangeText={(text) => this.setState({ email: text })}
              value={this.state.email}
              keyboardType="email-address"
            />
            <TextInput
              placeholder="Password"
              style={styles.inputBox}
              secureTextEntry={true}
              onChangeText={(text) => this.setState({ password: text })}
              value={this.state.password}
            />
            <Text style={styles.forgotPasword}>forgot password?</Text>
          </View>
          <TouchableOpacity 
            style={styles.button} 
            onPress={
              () => {
                if (this.state.email && this.state.password) {
                  alert('berhasil')
                } else alert('isi dulu')
              }
            } 
          >
            <Text style={styles.buttonText}>Login</Text>
          </TouchableOpacity>
          <View style={styles.signupTextCont}>
            <Text>Don't have account?</Text>
            <TouchableOpacity onPress={this.signup}><Text style={{color:primaryColor}}> Signup</Text></TouchableOpacity>
          </View>
      </View>
    )
  };
}
const primaryColor = '#2F80ED';
const styles = StyleSheet.create({  
  container: {
    padding: 15,
    flexGrow: 1,
    justifyContent:'flex-start',
    alignItems: 'center'
  },
  logoContainer: {
    flexDirection: 'row',
    justifyContent: "flex-end",
    marginVertical: 15
  },
  logoImage: {
    fontSize: 100,
    color: primaryColor
  },
  logoText: {
    fontSize: 40
  },
  inputBox: {
    width:300,
    borderWidth: 1,
    borderColor: primaryColor,
    borderRadius: 25,
    paddingHorizontal:20,
    paddingVertical:10,
    fontSize:16,    
    color:primaryColor,
    width:300,
    marginVertical: 10
  },
  button: {
    width:300,
    backgroundColor:primaryColor,
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13
  },
  buttonText: {
    fontSize:18,
    fontWeight:'500',
    color:'white',
    textAlign:'center'
  },
  forgotPasword: {
    textAlign: "right",
    color: primaryColor
  },
  signupTextCont: {
    flexDirection: 'row'
  }
});

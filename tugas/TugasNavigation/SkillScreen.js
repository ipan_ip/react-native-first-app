import React from 'react';
import CardSkill from '../TugasNavigation/components/CardSkill';
import skills from '../tugas14/Tugas14/skillData.json'
import { 
  View, 
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import CommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class PageSkills extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={{fontSize: 25, color: 'white'}}>Skills</Text>
          <Text style={{fontSize: 35, color: 'white'}}>Irfan Aziz Al Amin</Text>
        </View>
        <View style={styles.body}>
          <ScrollView horizontal style={styles.categoryBar}>
            <View style={styles.categoryItem}>
              <Text style={styles.categoryText}>Language</Text>
            </View>
            <View style={styles.categoryItem}>
              <Text style={styles.categoryText}>Library</Text>
            </View>
            <View style={styles.categoryItem}>
              <Text style={styles.categoryText}>Technology</Text>
            </View>
            <View style={styles.categoryItem}>
              <Text style={styles.categoryText}>Lainnya</Text>
            </View>
          </ScrollView>
          <FlatList 
            data={skills.items}
            renderItem={(skill) =>
              <CardSkill skill={skill.item}/>
            }
            keyExtractor={(item) => item.id}
            ItemSeparatorComponent={() => <View style={{
              marginVertical: 10
              // height: 5,
              // backgroundColor: 'black'
            }}></View>}
          />
        </View>
        <View style={styles.tabBar}>
          <TouchableOpacity style={styles.tabItem}>
            <CommunityIcon name="account-outline" size={25}></CommunityIcon>
            <Text style={styles.tabTitle}>Account</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name="directions-bike" style={{color: '#2F80ED'}} size={25}></Icon>
            <Text style={styles.tabTitle, {color: '#2F80ED'}}>Skills</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <CommunityIcon name="logout" size={25}></CommunityIcon>
            <Text style={styles.tabTitle}>Logout</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const primaryColor = '#2F80ED'
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: primaryColor,
    height: 100,
    padding: 10
  },
  body: {
    padding: 15,
    flex: 1
  },
  categoryBar: {
    flexDirection: 'row',
    marginBottom: 15
  },
  categoryItem: {
    backgroundColor: primaryColor,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 20,
    marginHorizontal: 10
  },
  categoryText: {
    marginHorizontal: 20,
    marginVertical: 8,
    color: 'white',
    fontSize: 14
  },
  tabBar: {
    flexDirection: 'row',
    justifyContent: "space-around",
    height: 60,
    backgroundColor: 'white',
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5'
  },
  tabItem: {
    alignItems: "center",
    justifyContent: "center"
  },
  tabTitle: {
    fontSize: 11,
    color: '#3C3C3C',
    paddingTop: 3
  }
});

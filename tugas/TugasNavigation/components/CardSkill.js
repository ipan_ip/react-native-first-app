import React from 'react';
import { 
    View, 
    Text,
    Image,
    ScrollView, 
    TextInput, 
    TouchableOpacity, 
    StyleSheet 
} from 'react-native';

function predicateName(progress) {
    var result = "";
    var checkProgress = progress.slice(0, progress.length - 1)
    if (checkProgress < 70) result = 'Beginner'
    else if (checkProgress >= 90) result = 'Advance'
    else result = 'Itermediete'
    return `${result} (${progress})`
}

export default class Note extends React.Component {
    render() {
        let skill = this.props.skill;
        return (
            <View style={styles.container}>
                <View>
                    <Image 
                        source={{uri:skill.logoUrl}}
                        style={styles.image}
                    />
                </View>
                <View>
                    <Text style={styles.skillName}>{skill.skillName}</Text>
                    <Text style={styles.category}>
                        <Text style={styles.title}>Category: </Text>
                        <Text style={styles.content}>{skill.category}</Text>
                    </Text>
                    <Text style={styles.prdicate}>
                        <Text style={styles.title}>Predicate: </Text>
                        <Text style={styles.content}>{predicateName(skill.percentageProgress)}</Text>
                    </Text>
                </View>
            </View>
        )
    }
};

const primaryColor = '#2F80ED'
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: "center",
        borderRadius: 10,
        borderColor: 'grey',
        borderWidth: 0.5,
        // backgroundColor: 'green'
    },
    image: {
        height: 80,
        width: 80,
        marginVertical: 20,
        marginHorizontal: 20
    },
    skillName: {
        fontSize: 30
    },
    category: {
        fontSize: 16,
    },
    predicate: {
        fontSize: 16,
    },
    title:{},
    content: {
        color: primaryColor
    }
});


import React from "react";
import { 
  StyleSheet, 
  Text, 
  View
} from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';

import { render } from "react-dom";
import LoginScreen from './LoginScreen'
import AboutScreen from './AboutScreen'
import SkillScreen from './SkillScreen'

const AuthStack = createStackNavigator();
const HomeStack = createStackNavigator();
const ProfileStack = createStackNavigator();
const SearchStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

// function App() {
//   return (
//       <LoginScreen />
//   );
// }

const AppNavigator = createStackNavigator({
  Home: {
    screen: SkillScreen
  }
})

export default createAppContainer(AppNavigator)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
